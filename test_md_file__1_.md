### This is sample markdown file

## sample text:

In **publishing and graphic design**, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. *Lorem ipsum may be used as a placeholder before final copy is available*.

1. this is the first point
     1. this is 1st in 1st
     2. thisis 1st in 2nd 
     3. this is 1st in 3rd
2. this is the second point 
3. this 3rd one 

- this is the first point
   - this is 1st in 1st
   - this is 1st in 2nd
   - this is 1st in 3rd
- this is the second point
- this 3rd one

## this is r code
```r
a = 4
print(a)
```

## Here is the some links 
I like [r language](https://www.r-project.org/other-docs.html)

# Here is the R logo:

![r logo](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/R_logo.svg/1086px-R_logo.svg.png)

<font color=blue>font color is blue </font>

<font color=red>font color is blue </font>






```R

```
